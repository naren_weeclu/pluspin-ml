import flask
from flask import Flask, jsonify, request
import sys, os
import json
import pickle
app = Flask(__name__)
gender_map = {'M': 1, 'F': 0}
bmi_map = {'Under Weight': 0,'Normal Weight': 1,'Over Weight': 2,'Obese': 3 }
hba1c_map = {'ADA Target': 3, 'Action Suggested': 4, 'Diabetic': 2, 'Non-Diabetic': 0, 'Pre-Diabetic': 1}
def load_models():
    file_name = "models/model_file.p"
    with open(file_name, 'rb') as pickled:
        data = pickle.load(pickled)
        model = data['model']
    return model
def getDiab(gender,age,height,weight):
    model = load_models()
    output = {}
    bmi_value = weight/height**2
    if bmi_value < 18.5:bmi_interp = 0
    elif bmi_value >= 18.5 and bmi_value < 24.9:bmi_interp = 1
    elif bmi_value >= 25 and bmi_value < 29.9:bmi_interp = 2
    else:bmi_interp = 3
    output['bmi'] = round(bmi_value)
    output['class'] = get_key(model.predict([[gender,age,height,weight,bmi_value,bmi_interp]]),hba1c_map )
    #print("you are in class:",get_key(model.predict([[gender,age,height,weight,bmi_value,bmi_interp]]),hba1c_map ))
    output['prob'] = getProba(model.predict_proba([[gender,age,height,weight,bmi_value,bmi_interp]])[0])
    return output
def get_key(val,my_dict):
    for key, value in my_dict.items(): 
         if val == value: 
             return key 
    return "invalid"   
def getProba(arr):
    probj = {}
    for i in range(0,len(arr)):
        probj[get_key(i,hba1c_map)] = arr[i]
    return probj
@app.route('/predict_diabetes', methods=['GET'])
def predict():
    try:
        # parse input features from request
        print(request)
        request_json = request.args
        gender = int(request_json['gender'])
        age =  int(request_json['age'])
        height =  float(request_json['height'])
        weight = float(request_json['weight'])
        prediction = getDiab(gender,age,height,weight) 
        response = json.dumps({'response': prediction})
        return response, 200
    except Exception as e:
        print(e)
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)
        response = json.dumps({'message':"server error",'response':None})
        return response, 200
if __name__ == '__main__':
     application.run(debug=True)
 